'use strict';

const gulp  = require('gulp');
const sync  = require('browser-sync').create();


gulp.task('default', ['compile', 'browser-sync'], () => {
  gulp.watch('src/**/*', ['compile']);
  gulp.watch('dist/**/*', ['reload']);
});

gulp.task('compile', () => {
  return gulp
    .src('src/**/*')
    .pipe(gulp.dest('dist/'));
});

gulp.task('browser-sync', function () {
  sync.init({
    server: {
      baseDir: 'dist'
    }
  });
});

gulp.task('reload', () => {
  sync.reload();
  return gulp;
});
