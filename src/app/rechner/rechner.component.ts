import { Component, OnInit } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';

interface valueTypes {
  gewGesMenge       : number,
  
  aromaPreis        : number,
  aromaInhalt       : number,
  aromaMisch        : number,
  aromaBen          : number,
  aromaBenPreis     : number,

  nikotinPreis      : number,
  nikotinInhalt     : number,
  nikotinShot       : number,
  nikotinGew        : number,
  nikotinBen        : number,
  nikotinBenPreis   : number,

  basePreis         : number,
  baseInhalt        : number,
  baseBen           : number,
  baseBenPreis      : number,

  fertigPreis       : number,
  fertigInhalt      : number,
  fertigBenPreis    : number,

  unterschiedPreis  : number,
  gesPreisSelbst    : any
}

@Component({
  selector: 'app-rechner',
  templateUrl: './rechner.component.html',
  styleUrls: ['./rechner.component.css']
})

export class RechnerComponent implements OnInit {
  valuesDefault : valueTypes = {
    gewGesMenge     : 100,

    aromaPreis      : 0,
    aromaInhalt     : 0,
    aromaMisch      : 0,
    aromaBen        : 0,
    aromaBenPreis   : 0,

    nikotinPreis    : 0,
    nikotinInhalt   : 0,
    nikotinShot     : 0,
    nikotinGew      : 0,
    nikotinBen      : 0,
    nikotinBenPreis : 0,

    basePreis       : 0,
    baseInhalt      : 0,
    baseBen         : 100,
    baseBenPreis    : 0,

    fertigPreis     : 0,
    fertigInhalt    : 0,
    fertigBenPreis  : 0,

    unterschiedPreis: 0,
    gesPreisSelbst  : 0
  };

  values : valueTypes = this.valuesDefault;

  showAlert = {
    hidden : true,
    show : ""
  };

  liquidKomp : string = "";

  toggleOption = {
    mischen         : "",
    mischenPreis    : "",
    fertigL         : "",
    hiddenPreis     : true,
    hiddenMischen   : true,
    hiddenFertigL   : true,
    hiddenGes       : true,
    hiddenHelp      : false
  };

  difference = {
    default : "Hier erscheint der Preisvergleich zwischen Fertigliquid und selbstgemischten Liquid, wenn alle benötigten Angaben gemacht wurden.",
    set     : ""
  }; 

  styleString : string = "";

  calc(){
    
    for ( let value in this.values ) {
      if ( 
        this.values[value] < 0        ||
        isNaN( this.values[value] )   ||
        this.values[value] === null
      ) {
       this.values[value] = 0;
      } else {
        this.roundToTwo( this.values[value] );
      }
    }

    this.values.aromaBen = this.roundToTwo( ( this.values.aromaMisch / 100 ) * this.values.gewGesMenge );
    if ( this.values.aromaInhalt > 0 ){
      this.values.aromaBenPreis = this.roundToTwo( ( this.values.aromaPreis / this.values.aromaInhalt ) * this.values.aromaBen  ); 
    } else {
      this.values.aromaBenPreis = 0;
    }

    if ( this.values.nikotinShot > 0 ){
      this.values.nikotinBen      = this.roundToTwo( ( this.values.nikotinGew * this.values.gewGesMenge ) / this.values.nikotinShot );
    } else {
      this.values.nikotinBen      = 0;
    }

    if ( this.values.nikotinInhalt > 0 ){
      this.values.nikotinBenPreis = this.roundToTwo( ( this.values.nikotinPreis / this.values.nikotinInhalt ) * this.values.nikotinBen  );  
    } else {
      this.values.nikotinBenPreis = 0;
    }
    
    this.values.baseBen = this.roundToTwo( this.values.gewGesMenge - this.values.aromaBen - this.values.nikotinBen );
    if ( this.values.baseBen < 0 ){
      this.alerterShow();
    }

    if ( this.values.baseInhalt > 0 ){
      this.values.baseBenPreis = this.roundToTwo( ( this.values.basePreis / this.values.baseInhalt ) * this.values.baseBen  );
    } else {
      this.values.baseBenPreis = 0;
    }

    if ( this.values.fertigInhalt > 0 ){
      this.values.fertigBenPreis = this.roundToTwo( ( this.values.fertigPreis / this.values.fertigInhalt ) * this.values.gewGesMenge );
    } else {
      this.values.fertigBenPreis = 0;
    }
    
    this.values.gesPreisSelbst = this.values.nikotinBenPreis + this.values.aromaBenPreis + this.values.baseBenPreis

    this.values.unterschiedPreis = this.roundToTwo( this.values.fertigBenPreis - this.values.gesPreisSelbst );
    
    this.setPriceDif();
  }

  ngOnInit(){
    this.difference.set = this.difference.default;
  }

  roundToTwo( num : number ) { 
    num = parseFloat( num.toFixed(2) );
    return num
  }

  alerterShow(){
    let komp = "";
    if ( this.values.gewGesMenge < this.values.aromaBen ){
      komp = "beim Aroma! Die benötigte Menge an Aroma ist größer als die angegebene Gesamtmenge"
    } else if ( this.values.gewGesMenge < this.values.nikotinBen ){
      komp = "beim Nikotin! Die benötigte Menge an Nikotin ist größer als die angegebene Gesamtmenge"
    } else if ( this.values.gewGesMenge < ( this.values.aromaBen + this.values.nikotinBen ) ) {
      komp = "beim Aroma und Nikotin! Die benötigte Menge an Aroma und Nikotin ist größer als die angegebene Gesamtmenge"
    }

    this.liquidKomp       = komp;
    this.showAlert.hidden = false;
    this.showAlert.show   = "show";
  }

  alerterDismiss(){
    this.showAlert.hidden = true;
    this.showAlert.show   = "";
  }

  toggleOptions(option){
    const y : string          = "active";
    const n : string          = "";
  
    switch (option) {
      case "mischen":
        if( this.toggleOption.mischen === n ){
          this.toggleOption.mischen       = y;
          this.toggleOption.mischenPreis  = n;
          this.toggleOption.hiddenPreis   = true;
          this.toggleOption.hiddenMischen = false;
          this.toggleOption.hiddenGes     = false;
        } else if (this.toggleOption.mischen === y) {
          this.toggleOption.mischen       = n;
          this.toggleOption.mischenPreis  = n;
          this.toggleOption.hiddenPreis   = true;
          this.toggleOption.hiddenMischen = true;
          if ( this.toggleOption.hiddenFertigL === true ){
            this.toggleOption.hiddenGes   = true;
          }
        }
        break;

      case "mischenPreis":
        if( this.toggleOption.mischenPreis === n ){
          this.toggleOption.mischenPreis  = y;
          this.toggleOption.mischen       = n;
          this.toggleOption.hiddenPreis   = false;
          this.toggleOption.hiddenMischen = false;
          this.toggleOption.hiddenGes     = false;
        } else if (this.toggleOption.mischenPreis === y) {
          this.toggleOption.mischenPreis  = n;
          this.toggleOption.mischen       = n;
          this.toggleOption.hiddenPreis   = true;
          this.toggleOption.hiddenMischen = true;
          if ( this.toggleOption.hiddenFertigL === true ){
            this.toggleOption.hiddenGes   = true;
          }
        }
        break;

      case "fertigL":
        if( this.toggleOption.fertigL === n ){
          this.toggleOption.fertigL       = y;
          this.toggleOption.hiddenFertigL = false;
          this.toggleOption.hiddenGes     = false;
        } else if (this.toggleOption.fertigL === y) {
          this.toggleOption.fertigL = n;
          this.toggleOption.hiddenFertigL = true;
          if (   
            this.toggleOption.hiddenPreis   === true &&
            this.toggleOption.hiddenMischen === true
          ){
            this.toggleOption.hiddenGes   = true;
          }
        }
        break;
    
      default:
        break;
    }

    if (
      this.toggleOption.hiddenPreis   === true &&
      this.toggleOption.hiddenMischen === true &&
      this.toggleOption.hiddenFertigL === true 
    ){
      this.toggleOption.hiddenHelp = false;
    } else {
      this.toggleOption.hiddenHelp = true;
    }

  }

  setPriceDif(){
    this.difference.set = this.difference.default;
    this.styleString    = "badInput";

    if (
      this.values.aromaBen      > 0 &&
      this.values.aromaBenPreis === 0
    ){
      this.values.gesPreisSelbst = "Aromapreis unvollständig";
    } else if (
      this.values.nikotinBen   > 0 &&
      this.values.nikotinBenPreis === 0
    ){
      this.values.gesPreisSelbst = "Nikotinpreis unvollständig";
    } else if (
      this.values.baseBen       > 0 &&
      this.values.baseBenPreis  === 0
    ){
      this.values.gesPreisSelbst = "Basenpreis unvollständig";
    } else {
      this.styleString = "";
      if (
        this.values.fertigBenPreis > 0 &&
        this.values.gesPreisSelbst > 0
      ){
        if ( this.values.unterschiedPreis > 0){
          this.difference.set = "Das selbstgemischte Liquid ist um " + this.values.unterschiedPreis + "€ günstiger als das Fertigliquid!";
        } else if ( this.values.unterschiedPreis < 0 ){
          this.values.unterschiedPreis = - this.values.unterschiedPreis;
          this.difference.set = "Das Fertigliquid ist um " + this.values.unterschiedPreis + "€ günstiger als das selbstgemischte Liquid!";
        } else {
          this.difference.set = "Fertigliquid und selbstgemischtes Liquid kosten gleich viel!";
        }
      } 
    }



  }

}
