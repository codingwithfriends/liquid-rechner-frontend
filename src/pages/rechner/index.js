class SGL {

  constructor() {
    /* VARIABLEN */
    // Gesamtmenge
    this.gesamtmenge                    = 100;
    //Aroma
    this.preisa                         = 0;
    this.inhalta                        = 0;
    this.misch                          = 0;
    this.benAroma                       = 0;
    this.preisgesaroma                  = 0;
    // Nikotin
    this.preisn                         = 0;
    this.inhaltn                        = 0;
    this.nikotinstaerke                 = 0;
    this.nikotinstaerkegew              = 0;
    this.benNikotin                     = 0;
    this.preisgesnikotin                = 0;
    // Base
    this.preisb                         = 0;
    this.inhaltb                        = 0;
    this.benBase                        = this.gesamtmenge;
    this.preisgesbase                   = 0;
    // Fertigliquid
    this.preisf                         = 0;
    this.inhaltf                        = 0;
    this.preisgesfertig                 = 0;
    // Preisvergleich
    this.preisvergleichwert             = "";
    //Gespreis - selbstgemischtes Liquid
    this.gespreisselbstgemischtesliquid = "";

    /* ELEMENTE */
    // Gesamtmenge
    this.gesamtmenge$                    = document.querySelector('#gesamtmenge');
    // Aroma
    this.preisa$                         = document.querySelector('#preis-aroma');
    this.inhalta$                        = document.querySelector('#inhalt-aroma');
    this.misch$                          = document.querySelector('#misch-aroma');
    this.benAroma$                       = document.querySelector('#ben-aroma');
    this.preisgesaroma$                  = document.querySelector('#preis-ges-aroma');
    // Nikotin
    this.preisn$                         = document.querySelector('#preis-nikotin');
    this.inhaltn$                        = document.querySelector('#inhalt-nikotin');
    this.nikotinstaerke$                 = document.querySelector('#staerke-nikotin');
    this.nikotinstaerkegew$              = document.querySelector('#gewstaerke-nikotin');
    this.benNikotin$                     = document.querySelector('#ben-nikotin');
    this.preisgesnikotin$                = document.querySelector('#preis-ges-nikotin');
    // Base
    this.preisb$                         = document.querySelector('#preis-base');
    this.inhaltb$                        = document.querySelector('#inhalt-base');
    this.benBase$                        = document.querySelector('#ben-base');
    this.preisgesbase$                   = document.querySelector('#preis-ges-base');
    // Fertigliquid
    this.preisf$                         = document.querySelector('#preis-fertig');
    this.inhaltf$                        = document.querySelector('#inhalt-fertig');
    this.preisgesfertig$                 = document.querySelector('#preis-ges-fertig');
    // Preisvergleich
    this.preisvergleichwert$             = document.querySelector('#preisvergleich');
    //Gespreis - selbstgemischtes Liquid
    this.gespreisselbstgemischtesliquid$ = document.querySelector('#gespreisselbstgemischtesliquid');

    // Button disable
    this.checkboxSGL$   = document.getElementById("checkbox-SGL");
    this.checkboxpreis$ = document.getElementById("checkbox-preis");

    // Reset button
    this.resetbutton$ = document.getElementById("reset-buttonJS");

    this.addEvents();
  }

  addEvents() {
    this.gesamtmenge$.addEventListener('keyup', this.onGesamtmengeChange.bind(this));
    this.preisa$.addEventListener('keyup', this.onPreisaChange.bind(this));
    this.inhalta$.addEventListener('keyup', this.onInhaltaChange.bind(this));
    this.misch$.addEventListener('keyup', this.onMischChange.bind(this));
    this.preisn$.addEventListener('keyup', this.onPreisnChange.bind(this));
    this.inhaltn$.addEventListener('keyup', this.onInhaltnChange.bind(this));
    this.nikotinstaerke$.addEventListener('keyup', this.onNikotinstaerkeChange.bind(this));
    this.nikotinstaerkegew$.addEventListener('keyup', this.onNikotinstaerkegewChange.bind(this));
    this.preisb$.addEventListener('keyup', this.onPreisbChange.bind(this));
    this.inhaltb$.addEventListener('keyup', this.onInhaltbChange.bind(this));
    this.preisf$.addEventListener('keyup', this.onPreisfChange.bind(this));
    this.inhaltf$.addEventListener('keyup', this.onInhaltfChange.bind(this));

    this.gesamtmenge$.addEventListener('change', this.onGesamtmengeChange.bind(this));
    this.preisa$.addEventListener('change', this.onPreisaChange.bind(this));
    this.inhalta$.addEventListener('change', this.onInhaltaChange.bind(this));
    this.misch$.addEventListener('change', this.onMischChange.bind(this));
    this.preisn$.addEventListener('change', this.onPreisnChange.bind(this));
    this.inhaltn$.addEventListener('change', this.onInhaltnChange.bind(this));
    this.nikotinstaerke$.addEventListener('change', this.onNikotinstaerkeChange.bind(this));
    this.nikotinstaerkegew$.addEventListener('change', this.onNikotinstaerkegewChange.bind(this));
    this.preisb$.addEventListener('change', this.onPreisbChange.bind(this));
    this.inhaltb$.addEventListener('change', this.onInhaltbChange.bind(this));
    this.preisf$.addEventListener('change', this.onPreisfChange.bind(this));
    this.inhaltf$.addEventListener('change', this.onInhaltfChange.bind(this));

    this.checkboxSGL$.addEventListener('click', this.oncheckboxSGLChange.bind(this));
    this.resetbutton$.addEventListener('click', this.onresetButtonClick.bind(this));

  }

  onGesamtmengeChange() {
    this.gesamtmenge = parseFloat(this.gesamtmenge$.value);
    this.benAromacalc();
    this.benNikotincalc();
    this.preisgesFertigcalc();
    this.preisvergleichCalc();
  }

  onPreisaChange() {
    this.preisa = parseFloat(this.preisa$.value);
    this.preisgesAromacalc();
    this.preisvergleichCalc();
  }

  onInhaltaChange() {
    this.inhalta = parseFloat(this.inhalta$.value);
    this.preisgesAromacalc();
    this.preisvergleichCalc();
  }

  onMischChange() {
    this.misch = parseFloat(this.misch$.value);
    this.benAromacalc();
    this.preisvergleichCalc();
  }

  onPreisnChange() {
    this.preisn = parseFloat(this.preisn$.value);
    this.preisgesNikotincalc();
    this.preisvergleichCalc();
  }

  onInhaltnChange() {
    this.inhaltn = parseFloat(this.inhaltn$.value);
    this.preisgesNikotincalc();
    this.preisvergleichCalc();
  }

  onNikotinstaerkeChange() {
    this.nikotinstaerke = parseFloat(this.nikotinstaerke$.value);
    this.benNikotincalc();
    this.preisvergleichCalc();
  }

  onNikotinstaerkegewChange() {
    this.nikotinstaerkegew = parseFloat(this.nikotinstaerkegew$.value);
    this.benNikotincalc();
    this.preisvergleichCalc();
  }

  onPreisbChange() {
    this.preisb = parseFloat(this.preisb$.value);
    this.preisgesBasecalc();
    this.preisvergleichCalc();
  }

  onInhaltbChange() {
    this.inhaltb = parseFloat(this.inhaltb$.value);
    this.preisgesBasecalc();
    this.preisvergleichCalc();
  }

  onPreisfChange() {
    this.preisf = parseFloat(this.preisf$.value);
    this.preisgesFertigcalc();
    this.preisvergleichCalc();
  }

  onInhaltfChange() {
    this.inhaltf = parseFloat(this.inhaltf$.value);
    this.preisgesFertigcalc();
    this.preisvergleichCalc();
  }

  oncheckboxSGLChange() {
    this.checkboxSGLChange();
  }

  onresetButtonClick() {
    this.resetButtonClick();
  }

  /* FUNKTIONEN FÜR BERECHNUNGEN */
  benAromacalc() {
    if (
      this.gesamtmenge !== undefined &&
      this.misch !== undefined &&
      isNaN(this.gesamtmenge) === false &&
      isNaN(this.misch) === false
    ) {
      this.benAroma = this.gesamtmenge * (this.misch / 100);
      if (
        this.benNikotin > this.gesamtmenge ||
        this.benAroma + this.benNikotin > this.gesamtmenge
      ) {
        alert("Eingaben prüfen ! Der eingegebene Wert ist nicht möglich, da das benötigte Aroma so die Gesamtmenge überschreitet !")
      }
      this.preisgesAromacalc();
    } else {
      this.benAroma = 0;
    }
    this.benBasecalc();
    this.benAroma$.value = this.benAroma.toFixed(2);
  }

  preisgesAromacalc() {
    if (
      this.preisa !== undefined &&
      this.inhalta !== undefined &&
      this.inhalta !== 0 &&
      isNaN(this.preisa) === false &&
      isNaN(this.inhalta) === false
    ) {
      this.preisgesaroma = this.benAroma * (this.preisa / this.inhalta);
    } else {
      this.preisgesaroma = 0;
    }
    this.preisgesaroma$.value = this.preisgesaroma.toFixed(2);
    this.gespreisselbstgemischtesliquidCalc();
  }

  benNikotincalc() {
    if (
      this.gesamtmenge !== undefined &&
      this.nikotinstaerke !== undefined &&
      this.nikotinstaerkegew !== undefined &&
      this.nikotinstaerke !== 0 &&
      isNaN(this.gesamtmenge) === false &&
      isNaN(this.nikotinstaerke) === false &&
      isNaN(this.nikotinstaerkegew) === false

    ) {
      this.benNikotin = ( 1 / this.nikotinstaerke ) * this.gesamtmenge * this.nikotinstaerkegew;
      if (
        this.benNikotin > this.gesamtmenge ||
        this.benAroma + this.benNikotin > this.gesamtmenge
      ) {
        alert("Eingaben prüfen ! Der eingegebene Wert ist nicht möglich, da das benötigte Nikotin so die Gesamtmenge überschreitet !")
      }
      this.preisgesNikotincalc();
    } else {
      this.benNikotin = 0;
    }

    this.benBasecalc();
    this.benNikotin$.value = this.benNikotin.toFixed(2);
  }

  preisgesNikotincalc() {
    if (
      this.preisn !== undefined &&
      this.inhaltn !== undefined &&
      this.inhaltn !== 0 &&
      isNaN(this.preisn) === false &&
      isNaN(this.inhaltn) === false
    ) {
      this.preisgesnikotin = this.benNikotin * (this.preisn / this.inhaltn);
    } else {
      this.preisgesnikotin = 0;
    }
    this.preisgesnikotin$.value = this.preisgesnikotin.toFixed(2);
    this.gespreisselbstgemischtesliquidCalc();
  }

  benBasecalc() {
    this.benBase = this.gesamtmenge - this.benAroma - this.benNikotin;
    if (
      this.benBase < 0
    ) {
    } else {
      this.benBase$.value = this.benBase.toFixed(2);
    }
  }

  preisgesBasecalc() {
    if (
      this.preisb !== undefined &&
      this.inhaltb !== undefined &&
      this.inhaltb !== 0 &&
      isNaN(this.preisb) === false &&
      isNaN(this.inhaltb) === false
    ) {
      this.preisgesbase = this.benBase * (this.preisb / this.inhaltb);
    } else {
      this.preisgesbase = 0;
    }
    this.preisgesbase$.value = this.preisgesbase.toFixed(2);
    this.gespreisselbstgemischtesliquidCalc();
  }

  preisgesFertigcalc() {
    if (
      this.preisf !== undefined &&
      this.inhaltf !== undefined &&
      this.inhaltf !== 0 &&
      isNaN(this.preisf) === false &&
      isNaN(this.inhaltf) === false
    ) {
      this.preisgesfertig = this.gesamtmenge * (this.preisf / this.inhaltf);
    } else {
      this.preisgesfertig = 0;
    }
    this.preisgesfertig$.value = this.preisgesfertig.toFixed(2);
  }

  checkboxSGLChange() {
    this.checkboxpreis$.disabled = !this.checkboxpreis$.disabled;
  }

  resetButtonClick() {
    // Gesamtmenge
    this.gesamtmenge                    = 100;
    //Aroma
    this.preisa                         = 0;
    this.inhalta                        = 0;
    this.misch                          = 0;
    this.benAroma                       = 0;
    this.preisgesaroma                  = 0;
    // Nikotin
    this.preisn                         = 0;
    this.inhaltn                        = 0;
    this.nikotinstaerke                 = 0;
    this.nikotinstaerkegew              = 0;
    this.benNikotin                     = 0;
    this.preisgesnikotin                = 0;
    // Base
    this.preisb                         = 0;
    this.inhaltb                        = 0;
    this.benBase                        = this.gesamtmenge;
    this.preisgesbase                   = 0;
    // Fertigliquid
    this.preisf                         = 0;
    this.inhaltf                        = 0;
    this.preisgesfertig                 = 0;
    // Preisvergleich
    this.preisvergleichwert             = "";
    //Gespreis - selbstgemischtes Liquid
    this.gespreisselbstgemischtesliquid = "";

    this.gespreisselbstgemischtesliquidCalc();
    this.preisvergleichCalc();
  }

  gespreisselbstgemischtesliquidCalc() {
    console.log("passt ?!");
    this.gespreisselbstgemischtesliquid = this.preisgesnikotin + this.preisgesaroma + this.preisgesbase;
    if (
      this.gespreisselbstgemischtesliquid > 0
    ) {
      this.gespreisselbstgemischtesliquid$.innerHTML = "Gesamtpreis: " + this.gespreisselbstgemischtesliquid.toFixed(2) + " €";
    } else {
      this.gespreisselbstgemischtesliquid$.innerHTML = "";
    }
  }

  preisvergleichCalc() {
    if (
      this.gespreisselbstgemischtesliquid > 0
    ) {
      this.preisvergleichwert = this.preisgesbase + this.preisgesnikotin + this.preisgesaroma - this.preisgesfertig;
      if (
        this.preisgesfertig != 0
      ) {
        if (
          this.preisvergleichwert < 0
        ) {
          this.preisvergleichwert            = this.preisvergleichwert * (-1);
          this.preisvergleichwert$.innerHTML = "Selbermischen ist in diesem Fall um " + this.preisvergleichwert.toFixed(2) + " € günstiger !"
        } else if (
          this.preisgesfertig === this.preisgesbase
        ) {
          this.preisvergleichwert$.innerHTML = "Selbermischen kostet in diesem Fall gleich viel wie das Fertigliquid !"
        } else {
          this.preisvergleichwert$.innerHTML = "Das Fertigliquid ist in diesem Fall um " + this.preisvergleichwert.toFixed(2) + " € günstiger !"
        }
      }
    } else {
      this.preisvergleichwert$.innerHTML = "";
    }
  }
}

